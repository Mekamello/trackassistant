package com.mekamello.trackassistant.ui;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mekamello.trackassistant.R;
import com.mekamello.trackassistant.adapters.TabPagerAdapter;
import com.mekamello.trackassistant.database.DataGpx;
import com.mekamello.trackassistant.ui.tools.NavigationController;

import java.util.ArrayList;

/**
 *
 */
public class HolderMainFragment extends Fragment implements TabLayout.OnTabSelectedListener, FileListFragment.OnDataGpxSelectedListener {

    private static final String GOOGLE_MAP_FRAGMENT_TAG  = "GoogleMapFragment";
    private static final String FILE_LIST_FRAGMENT_TAG  = "FileListFragment";

    private TabLayout mTabLayout;
    private TabPagerAdapter mTabAdapter;
    private ArrayList<android.support.v4.app.Fragment> mPages;
    private FileListFragment mFileList;
    private GoogleMapFragment mGoogleMap;
    private NavigationController mViewPager;

    public static HolderMainFragment newInstance() {
        return new HolderMainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mFileList = FileListFragment.newInstance();
        mGoogleMap = GoogleMapFragment.newInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_holder_main, container, false);

        mTabLayout = (TabLayout) rootView.findViewById(R.id.sliding_tabs);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.tab_tracks));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.tab_map));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mFileList.setOnDataGpxSelectedListener(this);

        mPages = new ArrayList<>();
        mPages.add(mFileList);
        mPages.add(mGoogleMap);

        mTabAdapter = new TabPagerAdapter(getActivity(), getActivity().getSupportFragmentManager(), mPages);
        mViewPager = (NavigationController) rootView.findViewById(R.id.pager);
        mViewPager.setAdapter(mTabAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));


        mTabLayout.setOnTabSelectedListener(this);

        return rootView;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());

        if (tab.getPosition() > 0) {
            mViewPager.setPagingEnabled(false);
            mGoogleMap.updateMapView();
        } else {
            mViewPager.setPagingEnabled(true);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onDataGpxSelected(DataGpx dataGpx) {
        mGoogleMap.setDataGpx(dataGpx);
        mTabLayout.getTabAt(1).select();
    }
}

