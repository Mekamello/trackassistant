package com.mekamello.trackassistant.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import com.mekamello.trackassistant.R;
import com.mekamello.trackassistant.database.DatabaseHelper;

public class MainActivity extends ActionBarActivity {
    private static final String HOLDER_FRAGMENT_TAG  = "HolderMainFragment";

    private HolderMainFragment mHolderFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DatabaseHelper.createInstance(this);

        setContentView(R.layout.activity_main);

        Toolbar mToolBar = (Toolbar) findViewById(R.id.toolbar);
        mToolBar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(mToolBar);

        mHolderFragment = (HolderMainFragment) getSupportFragmentManager().findFragmentByTag(HOLDER_FRAGMENT_TAG);

        if(mHolderFragment == null) {
            mHolderFragment = HolderMainFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(mHolderFragment, HOLDER_FRAGMENT_TAG).commit();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DatabaseHelper.releaseInstance();
    }

}
