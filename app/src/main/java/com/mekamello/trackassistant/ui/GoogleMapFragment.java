package com.mekamello.trackassistant.ui;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mekamello.trackassistant.R;
import com.mekamello.trackassistant.database.DataGpx;
import com.mekamello.trackassistant.parser.ParserGpx;
import com.mekamello.trackassistant.parser.callbacks.CallbackGpx;
import com.mekamello.trackassistant.tools.TrackInfo;
import com.mekamello.trackassistant.tools.FileTools;
import com.mekamello.trackassistant.tools.OldDataHandler;

import java.io.File;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class GoogleMapFragment extends Fragment implements OnMapReadyCallback{
    private static final String GOOGLE_MAP_FRAGMENT_TAG = "google_map_fragment";
    private static final String DATA_GPX_STORAGE_TAG = "DataGpxCache";

    private View rootView = null;
    private ParserGpx parser;
    private GoogleMap googleMap;
    private DataGpx mDataGpx;
    private ProgressBar mMapIndicator;
    private SupportMapFragment mSupportMapFragment;
    private OldDataHandler mOldData;


    public static GoogleMapFragment newInstance() {
        return new GoogleMapFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_google_map, container, false);
        mMapIndicator = (ProgressBar) rootView.findViewById(R.id.loadingTracks);
        mOldData = OldDataHandler.getInstance();
        createMapView();

        return rootView;
    }

    public void setDataGpx(DataGpx dataGpx) {
        mDataGpx = dataGpx;
    }

    public DataGpx getDataGpx() {
        return mDataGpx;
    }

    public void updateMapView() {
        if(mDataGpx != null) {
            if (mSupportMapFragment == null){
                createMapView();
                return;
            }

            if (googleMap == null){
                mSupportMapFragment.getMapAsync(this);
                return;
            }

            googleMap.clear();
            loadingFile(mDataGpx);
        }
    }

    private void createMapView(){
        try {
            FragmentManager fragmentManager = getChildFragmentManager();
            mSupportMapFragment = (SupportMapFragment) fragmentManager.findFragmentByTag("map");
            mSupportMapFragment.getMapAsync(this);
        } catch (NullPointerException exception){
            Log.e(GOOGLE_MAP_FRAGMENT_TAG, exception.toString());
        }
    }
    private void loadingTracks(List<TrackInfo> tracks) {
        mMapIndicator.setVisibility(View.INVISIBLE);

        if (tracks != null) {
            for (TrackInfo track : tracks) {
                loadingTrack(track);
            }
        }

        if (!mDataGpx.getPath().equals(mOldData.getPath())) {
            mOldData.setDataTrack(tracks);
            mOldData.setPath(mDataGpx.getPath());
        }
    }

    private void loadingTrack(TrackInfo track){
        if(googleMap == null)
            return;

        PolylineOptions polylineOptions = new PolylineOptions();
        track.setPolyline(googleMap.addPolyline(polylineOptions));

        MarkerOptions beginMarker = new MarkerOptions();
        beginMarker.position(track.getPolyline().getPoints().get(0));
        beginMarker.title(getString(R.string.marker_start_point) + " " + mDataGpx.getName());
        googleMap.addMarker(beginMarker);

        MarkerOptions endMarker = new MarkerOptions();
        endMarker.position(track.getPolyline().getPoints().get(track.getPolyline().getPoints().size()-1));
        endMarker.title(getString(R.string.marker_end_point) + " " + mDataGpx.getName());
        googleMap.addMarker(endMarker);

        //Перенос камеры к начальной точке трека
        CameraUpdate startActualTrack = CameraUpdateFactory.newLatLngZoom(beginMarker.getPosition(), 14);
        googleMap.animateCamera(startActualTrack);
    }

    private void loadingFile(DataGpx dataGpx) {
        File file = new File(dataGpx.getPath());
        String name = dataGpx.getName();

        //TODO Сделать сохранение trackPoints

        if (mOldData.getPath() != null && mOldData.getPath().equals(mDataGpx.getPath())) {
            loadingTracks(mOldData.getDataTrack());
            return;
        }

        if(FileTools.isExpectedExtension(name)) {
            mMapIndicator.setVisibility(View.VISIBLE);
            parser = new ParserGpx(new CallbackGpx() {
                @Override
                public void callbackResult(List<TrackInfo> gpx) {
                    if (gpx == null){
                        Toast.makeText(getActivity(), R.string.parsing_error, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    loadingTracks(gpx);
                }
            }, file, mMapIndicator);
            parser.execute();
        }
        else {
            Toast.makeText(getActivity(), R.string.file_is_not_expected_extension, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if(mDataGpx != null)
            loadingFile(mDataGpx);
    }

}
