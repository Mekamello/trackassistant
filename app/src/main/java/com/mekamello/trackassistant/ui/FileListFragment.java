package com.mekamello.trackassistant.ui;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mekamello.trackassistant.R;
import com.mekamello.trackassistant.adapters.GpxFileListAdapter;
import com.mekamello.trackassistant.database.DataGpx;
import com.mekamello.trackassistant.database.DatabaseHelper;
import com.mekamello.trackassistant.tools.FileTools;

import java.io.File;
import java.util.List;

/**
 * Created by User on 23.09.2015.
 */
public class FileListFragment extends Fragment  {
    private ListView mListView;
    private Button mAddTrack;
    private List<DataGpx> mGpxList;
    private DatabaseHelper mHelper = DatabaseHelper.getInstance();
    private GpxFileListAdapter mAdapter = null;
    private OnDataGpxSelectedListener mDataGpxSelectedListener;

    public static FileListFragment newInstance() {
        return new FileListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setOnDataGpxSelectedListener(OnDataGpxSelectedListener onDataGpxSelectedListener) {
        this.mDataGpxSelectedListener = onDataGpxSelectedListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_file_list, container, false);

        mListView = (ListView) rootView.findViewById(R.id.tracksList);
        mListView.setSelector(R.drawable.row_color_selector);
        mListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        mAddTrack = (Button) rootView.findViewById(R.id.addTrack);

        updateList();

        mAddTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findFile();
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                DataGpx gpxFile = mGpxList.get(position);
//                parent.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.selection_color));
//                mListView.setSelection(position);
                if (mDataGpxSelectedListener != null){
                    mDataGpxSelectedListener.onDataGpxSelected(gpxFile);
                }
            }

        });
        return rootView;
    }

    public void updateList() {
        mGpxList = mHelper.getDataGpxDAO().getAllDataGpx();
        mAdapter = new GpxFileListAdapter(getActivity(), mGpxList);
        mListView.setAdapter(mAdapter);
    }

    public void findFile(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == -1) {
            Uri uri = data.getData();
            File file = new File(uri.getPath());
            String fileName = file.getName();

            if (!FileTools.isExpectedExtension(fileName)){
                Toast.makeText(getActivity(), R.string.file_is_not_expected_extension, Toast.LENGTH_SHORT).show();
                return;
            }

            if(existingItem(file.getAbsolutePath())) {
                Toast.makeText(getActivity(), R.string.file_is_existing, Toast.LENGTH_SHORT).show();
                return;
            }

            DataGpx dataGpx = new DataGpx();
            dataGpx.setName(fileName);
            dataGpx.setPath(file.getAbsolutePath());
            mHelper.getDataGpxDAO().createDataGpx(dataGpx);
            updateList();
        }

    }

    public boolean existingItem(String filePath) {
        for(DataGpx dataGpx : mGpxList) {
            if (dataGpx.getPath().equals(filePath)) {
                return true;
            }
        }
        return false;
    }

     public interface OnDataGpxSelectedListener{
         void onDataGpxSelected(DataGpx dataGpx);
     }
}
