package com.mekamello.trackassistant.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mekamello.trackassistant.R;

import java.util.ArrayList;

/**
 * Created by User on 22.09.2015.
 */
public class TabPagerAdapter extends FragmentStatePagerAdapter {
    private int[] mTabTitles = {R.string.tab_tracks, R.string.tab_map};
    private Context mContext;

    final ArrayList<Fragment> mPages;

    public TabPagerAdapter (Context context, FragmentManager fm, ArrayList<Fragment> list) {
        super(fm);
        mContext = context;
        mPages = list;
    }
    @Override
    public Fragment getItem(int i) {
        return mPages.get(i);
    }

    @Override
    public int getCount() {
        return mPages.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getString(mTabTitles[position]);
    }
}