package com.mekamello.trackassistant.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mekamello.trackassistant.R;
import com.mekamello.trackassistant.database.DataGpx;
import com.mekamello.trackassistant.database.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 25.09.2015.
 */
public class GpxFileListAdapter extends ArrayAdapter<DataGpx> {
    @LayoutRes
    private static final int ID_LAYOUT_ITEM = R.layout.item_file_list;

    private List<DataGpx> mDataList;

    public GpxFileListAdapter(Context context, List<DataGpx> list) {
        super(context, ID_LAYOUT_ITEM, list);

        this.mDataList = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        ViewHolder holder;
        if(view == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(ID_LAYOUT_ITEM, null);

            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.tvFileName);
            holder.delete = (ImageButton) view.findViewById(R.id.deleteButton);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if((mDataList == null) || (mDataList.size() < (position + 1)))
            return view;

        DataGpx dataGpx = mDataList.get(position);

        boolean currentDataGpxIsNotEmpty = dataGpx != null && !dataGpx.getName().isEmpty();
        if(holder.name != null && holder.delete != null && currentDataGpxIsNotEmpty) {
            holder.name.setText(dataGpx.getName());
            holder.delete.setTag(dataGpx);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataGpx dataGpx = (DataGpx) v.getTag();
                    Toast.makeText(getContext(), dataGpx.getName() + " " + getContext().getString(R.string.file_delete), Toast.LENGTH_LONG).show();
                    DatabaseHelper.getInstance().getDataGpxDAO().deleteDataGpx(dataGpx);
                    remove(dataGpx);
                    notifyDataSetChanged();
                }
            });
        }

        return view;
    }

    public static class ViewHolder {
        public TextView name;
        public ImageButton delete;
    }
}
