package com.mekamello.trackassistant.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.mekamello.trackassistant.database.DataGpx;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 12.09.2015.
 */
public class DataGpxDAO extends BaseDaoImpl<DataGpx, Integer> {
    public DataGpxDAO(ConnectionSource connectionSource, Class<DataGpx> dataGPXClass)
        throws SQLException {
        super(connectionSource, dataGPXClass);
    }

    public List<DataGpx> getAllDataGpx() {
        try {
            return this.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void createDataGpx(DataGpx dataGpx) {
        try {
            create(dataGpx);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteDataGpx(DataGpx dataGpx) {
        try {
            delete(dataGpx);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
