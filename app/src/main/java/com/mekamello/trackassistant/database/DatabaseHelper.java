package com.mekamello.trackassistant.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.mekamello.trackassistant.database.dao.DataGpxDAO;
import com.mekamello.trackassistant.database.tools.DatabaseSettings;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by User on 12.09.2015.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_HELPER_TAG = "DatabaseHelper";
    private static DatabaseHelper instance;

    private DataGpxDAO dataGpxDAO;

    public static DatabaseHelper getInstance(){
        if (instance == null){
            throw new RuntimeException("DatabaseHelper is not created");
        }

        return instance;
    }

    public static void createInstance(Context context){
        instance = new DatabaseHelper(context);
    }

    public static void releaseInstance(){
        OpenHelperManager.releaseHelper();
        instance = null;
    }

    private DatabaseHelper(Context context) {
        super(context, DatabaseSettings.DATABASE_NAME, null, DatabaseSettings.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, DataGpx.class);
        } catch (Exception e) {
            Log.e(DATABASE_HELPER_TAG, "Error creating db" + e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, DataGpx.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            Log.e(DATABASE_HELPER_TAG, "Error upgrating db" + e.getMessage());
        }
    }

    @Override
    public void close() {
        super.close();
        dataGpxDAO = null;
    }

    public DataGpxDAO getDataGpxDAO(){
        if(dataGpxDAO == null) {
            try {
                dataGpxDAO = new DataGpxDAO(getConnectionSource(),DataGpx.class);
            } catch (SQLException e) {
                Log.e(DATABASE_HELPER_TAG, "Error creating dataGpxDAO" + e.getMessage());
            }
        }
        return dataGpxDAO;
    }
}
