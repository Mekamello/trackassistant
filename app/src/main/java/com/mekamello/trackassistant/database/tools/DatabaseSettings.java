package com.mekamello.trackassistant.database.tools;

/**
 * Created by User on 12.09.2015.
 */
public class DatabaseSettings {
    public static final String DATABASE_NAME = "track_files.db";
    public static final int DATABASE_VERSION = 3;
}
