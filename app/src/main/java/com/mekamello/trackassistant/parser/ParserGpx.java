package com.mekamello.trackassistant.parser;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.mekamello.trackassistant.parser.callbacks.CallbackGpx;
import com.mekamello.trackassistant.tools.TrackInfo;
import com.urizev.gpx.GPXParser;
import com.urizev.gpx.beans.GPX;
import com.urizev.gpx.beans.Track;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 12.09.2015.
 */
public class ParserGpx extends AsyncTask<Void,Void,Void> {
    private static final String PARSER_GPX_TAG  = "PARSING";

    private GPXParser parser;
    private GPX gpx;
    private InputStream fileStream;
    private CallbackGpx callbackGpx;
    private View mLoader;

    @Override
    protected void onPreExecute() {
        mLoader.setVisibility(View.VISIBLE);
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            gpx = parser.parseGPX(fileStream);
        } catch (Exception e) {
            Log.e(PARSER_GPX_TAG, "Parsing error: " + e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        List<TrackInfo> tracks = new ArrayList<>();

        if (gpx != null)
            for(Track track :  gpx.getTracks()) {
                tracks.add(new TrackInfo(track));
            }

        callbackGpx.callbackResult(tracks);
        mLoader.setVisibility(View.GONE);
    }

    public ParserGpx(CallbackGpx callbackGpx, File file, View view) {
        mLoader = view;
        parser = new GPXParser();
        this.callbackGpx = callbackGpx;
        try {
            this.fileStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
