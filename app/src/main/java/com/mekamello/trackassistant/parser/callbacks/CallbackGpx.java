package com.mekamello.trackassistant.parser.callbacks;

import com.mekamello.trackassistant.tools.TrackInfo;

import java.util.List;

/**
 * Created by User on 12.09.2015.
 */
public interface CallbackGpx {
    void callbackResult(List<TrackInfo> gpx);
}
