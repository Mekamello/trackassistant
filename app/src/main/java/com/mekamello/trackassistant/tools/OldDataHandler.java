package com.mekamello.trackassistant.tools;

import java.util.List;

/**
 * Created by Mekamello on 05.10.2015.
 */
public class OldDataHandler {
    private static OldDataHandler mInstance;

    private List<TrackInfo> mTrackInfo;
    private String mPath;

    public static OldDataHandler getInstance() {
        if(mInstance == null) {
            mInstance = new OldDataHandler();
        }

        return mInstance;
    }

    private OldDataHandler() {}


    public List<TrackInfo> getDataTrack() {
        return mTrackInfo;
    }

    public void setDataTrack(List<TrackInfo> mTrackInfo) {
        this.mTrackInfo = mTrackInfo;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String mPath) {
        this.mPath = mPath;
    }
}
