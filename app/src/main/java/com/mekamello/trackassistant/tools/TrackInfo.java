package com.mekamello.trackassistant.tools;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.urizev.gpx.beans.Track;
import com.urizev.gpx.beans.Waypoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mekamello on 05.10.2015.
 */
public class TrackInfo {

    private Track mTrack;

    private List<LatLng> mTrackPoints;
    private Polyline mPolyline;

    public TrackInfo(Track track) {
        mTrack = track;
        mTrackPoints = new ArrayList<>();
        List<Waypoint> waypoints = track.getTrackPoints();
        for (Waypoint trackPoint : waypoints) {
            mTrackPoints.add(new LatLng(trackPoint.getLatitude(), trackPoint.getLongitude()));
        }
    }

    public List<LatLng> getTrackPoints() {
        return mTrackPoints;
    }

    public void setTrackPoints(List<LatLng> mTrackPoints) {
        this.mTrackPoints = mTrackPoints;
    }

    public Polyline getPolyline() {
        return mPolyline;
    }

    public void setPolyline(Polyline polyline) {
        this.mPolyline = polyline;
        mPolyline.setPoints(mTrackPoints);

    }
}
