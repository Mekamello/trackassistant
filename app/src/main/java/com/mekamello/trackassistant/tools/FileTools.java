package com.mekamello.trackassistant.tools;

/**
 * Created by User on 26.09.2015.
 */
public class FileTools {
    public static final String EXPECTED_EXTENSION = "gpx";
    public static boolean isExpectedExtension(String fileName) {
        String extension = fileName.substring((fileName.lastIndexOf(".") + 1), fileName.length());
        return (extension.toLowerCase().equals(EXPECTED_EXTENSION));
    }

}
